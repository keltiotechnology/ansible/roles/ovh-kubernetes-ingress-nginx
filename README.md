# Ovh kubernetes ingress nginx

Install ingress nginx on OVH kubernetes cluster.

## Vars
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| ovh_kubernetes_cluster_path_kubeconfig | Path to cluster kubeconfig | `string` | `` | yes |
| ovh_kubernetes_ingress_nginx_state | Decide if should install or uninstall ingress nginx | `Enum[present, absent]` | `present` | no |
